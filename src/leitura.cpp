/**
 * @file	leitura.cpp
 * @brief	Arquivo com a função que lê os dados de um arquivo de texto e os aloca em uma estrutura de dados.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	06/04/2017
 * @date	10/04/2017
 */

#include <iostream>
using std:: cout;
using std:: endl;
using std:: cerr;

#include <fstream>
using std:: ifstream;

#include <string>
using std:: string;

#include "stats.h"
#include "leitura.h"

 /**
 * @brief Função que realiza a leitura dos dados de um arquivo.
 * @param *argv Vetor de argumentos no qual está o arquivo a ser lido.
 * @param municipios Variavel que recebe a quantidade de municipios do arquivo.
 * @param stats Vetor de struct para o qual será alocado os dados que serão lidos do arquivo.
 */
void leitura_arquivo (char *argv, int municipios, Stats stats [] ) {

    ifstream entrada (argv);                       // tabela de dados serão passados pela linha de comando;

    string temp, leitura;

    getline (entrada, temp);                        // ler a primeira linha interia para "ignorar" o cabeçário

for (int jj = 0; jj < municipios; jj++) {

        unsigned int ii = 0;                        // contador

        while (getline (entrada, leitura)) {

            temp = "";                              // Variavel auxiliar

            for(ii = 1; ii < leitura.size(); ii++) { // ii começa de 1 para 'esquecer' as aspas.
                if (leitura[ii] != ' ') {            // se não for um espaço em branco
                    temp += leitura[ii];             // faço a concatenação para obter o codigo
                }
                else {                               // encontrou em espaço?
                    
                    stats[jj].codigo = temp;         // passa o código para a struct
                    temp = "";                       // "esvazio" para reutilizar 'temp'
                    
                    break;                           // encontrou um espaço, para!
                }
            }

            for (ii += 1; ii < leitura.size(); ii++) { // continua a leitura depois do espaço
                if (leitura[ii] != '"') {              // realiza a leitura até encontrar as proximas aspas
                    temp += leitura[ii];               // concaterna para obter o nome do municipio  
                }
                else {

                    stats[jj].nome = temp;
                    temp = "";                          // "esvazio" para reutilizar
                    break;
                }            
            }
            int cont = 0;
            for (ii += 2; ii < leitura.size(); ii++) {  // percorro o resto da linha obtida, depois de já ter alocado o nome e codigo no struct
                
                if (leitura[ii] != ';') {               // lê o numero até chegar no ';'
                    temp += leitura[ii];                // concaterna para obter a quantidade de nascimentos
                }
                else if (cont < 21) {
                        
                        stats[jj].nascimentos[cont] = atoi(temp.c_str());  // converte a string para int e aloca o numero na struct
                        
                        cont++;                                     // incrementa para ir para o próximo indice do vetor de nascimentos no struct
                        temp = "";                                  // "zero" o temp para que receba outro numero
                }
                else {                                              // após ler os 21 anos, não precisa mais continuar a leitura.
                    break;
                }
            }
            break;
        }
    }
    entrada.close ();
}