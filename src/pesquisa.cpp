/**
 * @file	pesquisa.cpp
 * @brief	Arquivo com a função de pesquisa.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	13/04/1017
 * @date	13/04/1017
 */

#include <iostream>
using std:: cout;
using std:: endl;
using std:: cerr;

#include <fstream>
using std:: ifstream;

#include <string>
using std:: string;

#include "pesquisa.h"

 /**
 * @brief Função que lê e aloca em um vetor os dados de um arquivo para a pesquisa.
 * @param *municipios_para_pesq Ponteiro que aponta para um vetor, no qual será alocado os dados contidos no arquivo.
 * @param qtd_municipios Variavel que recebe a quantidade de municipios que há no arquivo.. 
 */
void pesquisa (int *municipios_para_pesq, int qtd_municipios) {

    ifstream entrada ("./data/extra/alvos.dat");

    for (int ii = 0; ii < qtd_municipios; ii++) {
        string aux = "";
        getline (entrada, aux);
        municipios_para_pesq [ii] = atoi (aux.c_str() );
    }
    
    entrada.close();
}