/**
 * @file	main.cpp
 * @brief	Arquivo principal do programa que realiza operacoes sobre um vetor de diferentes tipos de dados.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	06/04/2017
 * @date	06/04/2017
 */

#include <iostream>
using std:: cout;
using std:: cin;
using std:: endl;
using std:: cerr;

#include <fstream>
using std:: ifstream;

#include <string>
using std:: string;

#include <cstdlib>
#include <unistd.h>

#include "stats.h"
#include "leitura.h"
#include "contador.h"
#include "calculos.h"
#include "gera_arquivos.h"
#include "pesquisa.h"

 /**
 * @brief Função principal do programa que realiza operacoes sobre um vetor de diferentes tipos de dados e gera um grafico com os mesmos.
 * @param argc Variavel com a quantidade de argumentos passados pelo terminal.
 * @param *argv Vetor de argumentos no qual está o arquivo a ser lido. 
 */
int main (int argc, char *argv[]) {

    Stats *stats;

    int municipios = contador (argv);

    stats = new Stats[municipios];

    int maior[21], menor[21], total[21];

    float desvio[21], media[21], *taxa_de_crescimento;
    taxa_de_crescimento = new float [municipios];
    
    leitura_arquivo (argv[1], municipios, stats);

    maior_ano (maior, stats, municipios);
    menor_ano (menor, stats, municipios);
    media_ano (media, stats, municipios);
    desvio_padrao_ano (media,desvio, stats, municipios);
    total_nascimentos_ano (total,stats, municipios);
    
    gera_arquivos (maior, menor, media, desvio, total);
    cout << ". . . Arquivo 'estatisticas.csv' gerado" << endl;
    cout << ". . . Arquivo 'totais.dat' gerado" << endl << endl;

    float menor_taxa = 0.0, maior_taxa = 0.0;
    int indice_maior = 0, indice_menor = 0;
    taxa (taxa_de_crescimento, stats, municipios, &menor_taxa, &maior_taxa, &indice_maior, &indice_menor);

    cout.precision(4);
    cout << "Municipio com maior taxa de queda 2013-2014: " << stats[indice_menor].nome << " (" << menor_taxa << "%)" << endl;
    cout << "Municipio com maior taxa de crescimento 2013-2014: " << stats[indice_maior].nome << " (+" << maior_taxa << "%)" << endl << endl;

    int *municipios_para_pesq;
    int qtd_municipios = contador_de_municipios_para_pesq ();
    municipios_para_pesq = new int[qtd_municipios];

    pesquisa (municipios_para_pesq, qtd_municipios);
    gera_pesquisa (municipios_para_pesq, qtd_municipios, taxa_de_crescimento , municipios, stats);

    cout << ". . . . . .[" << qtd_municipios << "] municipios definidos como alvo" << endl;
    for (int ii = 0; ii < qtd_municipios; ii++) {
        
        for (int jj = 0; jj < municipios; jj++) {

            if (municipios_para_pesq[ii] == atoi (stats[jj].codigo.c_str()) ) {
                cout << ". . . . . . { " << stats[jj].nome << " }" << endl;
            }
        }
    }

    delete [] taxa_de_crescimento;
    delete [] municipios_para_pesq;
    delete [] stats;

    cout << ". . . Arquivo 'extra.dat' gerado" << endl << endl;

    execlp("gnuplot","gnuplot","./scripts/plot","./scripts/plot_pesquisa",NULL);

    return 0;
}