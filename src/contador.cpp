/**
 * @file	contador.cpp
 * @brief	Arquivo com a função que "conta" a quantidade de municipios do arquivo.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	06/04/2017
 * @date	06/04/2017
 */

#include <iostream>
using std:: endl;
using std:: cerr;

#include <fstream>
using std:: ifstream;

#include <string>
using std:: string;

#include "contador.h"

 /**
 * @brief Função que calcula a quantidade de municipios que estão no arquivo.
 * @param *argv Vetor de argumentos no qual está o arquivo a ser lido.
 * @return retorna a quantidade de municipios.
 */
int contador (char *argv[]) {

    int cont = 0;

    ifstream entrada (argv[1]);

    if (!argv[1]) {
        cerr << "O arquivo não foi aberto" << endl;
        return 0;
    }

    while (!entrada.eof()) {
        string aux;
        getline(entrada,aux);
        cont++;
    }

    entrada.close();

    return cont-2;
 }

 /**
 * @brief Função que calcula a quantidade de municipios que estão no arquivo para a pesquisa.
 * @return retorna a quantidade de municipios.
 */
 int contador_de_municipios_para_pesq () {

    int cont = 0;
    ifstream entrada ("./data/extra/alvos.dat");

    while (!entrada.eof()) {
        string aux;
        getline(entrada,aux);
        cont++;
    }

    entrada.close();

    return cont-1;
}