/**
 * @file	calculos.cpp
 * @brief	Arquivo com as funções para o calculo estatisticos. 
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	08/04/1017
 * @date	12/04/1017
 */

 #include <cmath>

 #include "stats.h"

 /**
 * @brief Função que calcula o total de nascimentos em cada municipio durante 21 anos.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void total_nascimentos_municipios (int *total, Stats stats[], int municipios) {

     for (int ii = 0; ii < municipios; ii++) {
         int soma = 0;
         for (int jj = 0; jj < 21; jj++){
             soma += stats[ii].nascimentos[jj];
         }
         total[ii] = soma;
     }
     
 }

 /**
 * @brief Função que calcula o total de nascimentos em cada ano em todos os municipios.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void total_nascimentos_ano (int *total, Stats stats[], int municipios) {
     
     for (int ii = 0; ii < 21; ii++) {
         int soma = 0;
         for (int jj = 0; jj < municipios; jj++) {
             soma += stats[jj].nascimentos[ii];
         }
         total[ii] = soma;
     }
 }

 /**
 * @brief Função que calcula o maior numero de nascimentos no ano em todos os municipios.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void maior_ano (int *vetor,Stats stats[], int municipios) {

     for (int ii = 0; ii < 21; ii++) {
         vetor[ii] =  stats[0].nascimentos[ii];
         for (int jj = 0; jj < municipios; jj++) {
             if (vetor[ii] < stats[jj].nascimentos[ii]) {
                 vetor[ii] = stats[jj].nascimentos[ii];
             }
         }
     }
 }

  /**
 * @brief Função que calcula o menor numero de nascimentos no ano em todos os municipios.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void menor_ano (int *vetor,Stats stats[], int municipios) {

     for (int ii = 0; ii < 21; ii++) {
         vetor[ii] =  stats[0].nascimentos[ii];
         for (int jj = 0; jj < municipios; jj++) {
             if (vetor[ii] > stats[jj].nascimentos[ii]) {
                 vetor[ii] = stats[jj].nascimentos[ii];
             }
         }
     }
 }

  /**
 * @brief Função que calcula o valor medio do numero de nascimentos no ano em todos os municipios.
 * @param *medias Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void media_ano (float *medias, Stats stats[], int municipios) {
     
     for (int ii = 0; ii < 21; ii++) {
         float soma = 0.0;
         for (int jj = 0; jj < municipios; jj++) {
             soma += stats[jj].nascimentos[ii];
         }
         medias[ii] = soma/municipios;
     }
 }

  /**
 * @brief Função que calcula o desvio padrão do numero de nascimentos no ano em todos os municipios.
 * @param *desvio Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void desvio_padrao_ano (float *media, float *desvio, Stats stats[], int municipios) {
    
    float somatorio;
     for (int ii = 0; ii < 21; ii++) {
        somatorio = 0.0;
         for (int jj = 0; jj < municipios; jj++) {
            somatorio += (float) pow ( (stats[jj].nascimentos[ii] - media[ii]), 2);
         }
         desvio[ii] = sqrt( ( ((float)1/(float)municipios) * (float) somatorio) );
     }
 }

 /**
 * @brief Função que calcula a taxa de crescimento relativa de cada municipio.
 * @param *tc Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 * @param *menor_taxa Ponteiro que recebe o endereço de uma variavel para alocar o valor do calculo da menor taxa.
 * @param *maior_taxa Ponteiro que recebe o endereço de uma variavel para alocar o valor do calculo da maior taxa.
 * @param *indice_maior Ponteiro que recebe o endereço de uma variavel para alocar indice de onde o municipio com a maior taxa esta no struct.
 * @param *indice_menor Ponteiro que recebe o endereço de uma variavel para alocar indice de onde o municipio com a menor taxa esta no struct.
 */
 void taxa (float *tc, Stats stats[], int municipios, float *menor_taxa, float *maior_taxa, int *indice_maior, int *indice_menor) {

    for (int jj = 0; jj < municipios; jj++) {
        
        if (stats[jj].nascimentos[19] != 0) {
            tc[jj] = (float) 100 * ( (float) ( (float) stats[jj].nascimentos[20] / (float) stats[jj].nascimentos[19] ) - 1 );
        }

    }

    *menor_taxa = tc[0];
    *maior_taxa = tc[0];

    for (int jj = 1; jj < municipios; jj++) {
        
        if (*menor_taxa > tc[jj]) {

            *menor_taxa = tc[jj];
            *indice_menor = jj; 
        }

        if (*maior_taxa < tc[jj]) {

            *maior_taxa = tc[jj];
            *indice_maior = jj;
        }
    }
    
 }