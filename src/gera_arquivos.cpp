/**
 * @file	gera_arquivos.cpp
 * @brief	Arquivo com a função para gerar arquivos de texto.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	08/04/1017
 * @date	09/04/1017
 */

#include <iostream>
using std:: endl;
using std:: cout;

#include <fstream>
using std:: ofstream;

#include <iomanip>
using std:: setw;

#include <string>
using std:: string;

#include "gera_arquivos.h"
#include "stats.h"

 /**
 * @brief Função que gera arquivos com os resultados dos calculos.
 * @param *maior Ponteiro que recebe um vetor com o maior numero de nascimentos em cada ano.
 * @param *menor Ponteiro que recebe um vetor com o menor numero de nascimentos em cada ano.
 * @param *desvio Ponteiro que recebe um vetor com o desvio padrão do numero de nascimentos em cada ano.
 * @param *total Ponteiro que recebe um vetor com o total de nascimentos em cada ano.
 */
void gera_arquivos (int *maior, int *menor, float *media, float *desvio, int *total) {

    ofstream estatisticas;
    estatisticas.open("./data/estatisticas.csv");

    estatisticas << "Anos;Maior;Menor;Media;Desvio" << endl;
    for (int ii = 0; ii < 21; ii++) {
        estatisticas << 1994 + ii << ";" << maior[ii] << ";" << menor[ii] << ";" << media[ii] << ";" << desvio[ii] << endl; 
    }

    estatisticas.close();

    ofstream totais;
    totais.open("./data/totais.dat");

    totais << "Anos" << setw(10) << "Totais" << endl;
    for (int jj = 0; jj < 21; jj++) {
        totais << 1994 + jj << setw(10) << total[jj] << endl;
    }

    totais.close();

}

 /**
 * @brief Função que gera arquivos com os resultados da pesquisa.
 * @param *municipios_para_pesq Ponteiro que recebe um vetor com os municipios que estão no arquivo 'alvo.dat'.
 * @param qtd_municipios Variavel que recebe a quantidade de municipios que estão no arquivo 'alvo.dat'.
 * @param *tc Ponteiro que recebe um vetor com as taxas de crescimento.
 * @param municipios Variavel que recebe a quantidade de municipios contido no arquivo 'Nascimentos_RN.csv'.
 * @param stats[] Vetor que recebe a strutura de dados dos municipios.
 */
void gera_pesquisa (int *municipios_para_pesq, int qtd_municipios, float *tc, int municipios, Stats stats[]) {
    
    cout << ". . . Lendo arquivo 'alvos.dat'" << endl << endl;

    ofstream pesquisa;
    pesquisa.open ("./data/extra/extra.dat");

    pesquisa << "Ano" << setw(30) << "Taxa de crescimento" << endl;

    for (int ii = 0; ii < 21; ii++) {

        pesquisa << 1994 + ii;
        
        for (int jj = 0; jj < qtd_municipios; jj++) {

            for (int pp = 0; pp < municipios; pp++) {

                if (municipios_para_pesq[jj] == atoi (stats[pp].codigo.c_str()) ) {
                    pesquisa << setw(10) << tc[ii] - pp;
                }

            }
        }
        pesquisa << endl;
    }

    pesquisa.close();
}