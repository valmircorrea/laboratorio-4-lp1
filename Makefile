# Malefile for "laboratório- 03" C++ application
# Created by Valmir Correa 06/04/2016

RM = rm -rf

# Compilador:
CC = g++

# Variaveis para os subdiretorios:

INC_DIR=./include
SRC_DIR=./src
OBJ_DIR=./build
BIN_DIR=./bin
DOC_DIR=./doc
DAT_DIRC=./data
IMG_DIR=./images
TEST_DIR=./test
IMG_DIR = ./images

# Opcoes de compilacao:
CFLAGS = -Wall -pedantic -ansi -std=c++17 -I. -I$(INC_DIR)

# Assegura que os alvos não sejam confundidos com os arquivos de mesmo nome:
.PHONY: all clean distclean doxy

# Define o alvo para a compilação completa:
all: nascimentos

debug: CFLAGS += -g -O0
debug: nascimentos

# Alvo para a contrução do executavel:
nascimentos: $(OBJ_DIR)/main.o $(OBJ_DIR)/contador.o $(OBJ_DIR)/leitura.o $(OBJ_DIR)/calculos.o $(OBJ_DIR)/gera_arquivos.o $(OBJ_DIR)/pesquisa.o
	@echo
	@echo "================================================================================================"
	@echo  "                                Ligando o alvo '$@' "
	@echo "================================================================================================"
	@echo
	$(CC) $(CFLAGS) -o $(BIN_DIR)/$@ $^
	@echo
	@echo "================================================================================================"
	@echo "                      +++ [Executavel 'nascimentos' criado em $(BIN_DIR)] +++"
	@echo "================================================================================================"
	@echo

# Alvo para a construção do main.o:
$(OBJ_DIR)/main.o: $(SRC_DIR)/main.cpp
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do leitura.o:
$(OBJ_DIR)/leitura.o: $(SRC_DIR)/leitura.cpp $(INC_DIR)/leitura.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do contador.o:
$(OBJ_DIR)/contador.o: $(SRC_DIR)/contador.cpp $(INC_DIR)/contador.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a construção do calculos.o:
$(OBJ_DIR)/calculos.o: $(SRC_DIR)/calculos.cpp $(INC_DIR)/calculos.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução do gera_arquivo.o:
$(OBJ_DIR)/gera_arquivos.o: $(SRC_DIR)/gera_arquivos.cpp $(INC_DIR)/gera_arquivos.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a contrução da pesquisa.o:
$(OBJ_DIR)/pesquisa.o: $(SRC_DIR)/pesquisa.cpp $(INC_DIR)/pesquisa.h
	$(CC) -c $(CFLAGS) -o $@ $<

# Alvo para a execução do Valgrind:
valgrind:
	valgrind --leak-check=full --show-reachable=yes ./bin/nascimentos

# Alvo para a geração automatica de documentacao usando o Doxygen:
doxy:
	$(RM) $(DOC_DIR)/*
	doxygen Doxyfile
doxyg:
	doxygen -g

# Alvo usado para limpar os arquivos temporarios (objeto):
clean:
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/*
	$(RM) $(IMG_DIR)/*
