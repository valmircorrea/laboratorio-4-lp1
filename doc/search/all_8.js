var searchData=
[
  ['taxa',['taxa',['../calculos_8h.html#ad335fbcdb8fa497595ef870c170b5606',1,'taxa(float *tc, Stats stats[], int municipios, float *menor_taxa, float *maior_taxa, int *indice_maior, int *indice_menor):&#160;calculos.cpp'],['../calculos_8cpp.html#ad335fbcdb8fa497595ef870c170b5606',1,'taxa(float *tc, Stats stats[], int municipios, float *menor_taxa, float *maior_taxa, int *indice_maior, int *indice_menor):&#160;calculos.cpp']]],
  ['total_5fnascimentos_5fano',['total_nascimentos_ano',['../calculos_8h.html#aeded1ccd159eaf7cd792e67a458cb651',1,'total_nascimentos_ano(int *total, Stats stats[], int municipios):&#160;calculos.cpp'],['../calculos_8cpp.html#aeded1ccd159eaf7cd792e67a458cb651',1,'total_nascimentos_ano(int *total, Stats stats[], int municipios):&#160;calculos.cpp']]],
  ['total_5fnascimentos_5fmunicipios',['total_nascimentos_municipios',['../calculos_8h.html#aefe54b4b562bb7c5da1ba776ae3e33b9',1,'total_nascimentos_municipios(int *total, Stats stats[], int municipios):&#160;calculos.cpp'],['../calculos_8cpp.html#aefe54b4b562bb7c5da1ba776ae3e33b9',1,'total_nascimentos_municipios(int *total, Stats stats[], int municipios):&#160;calculos.cpp']]]
];
