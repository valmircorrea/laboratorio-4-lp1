/**
 * @file	pesquisa.h
 * @brief	Arquivo com o cabeçário da função leitura_arquivo.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	06/04/2017
 * @date	10/04/2017
 */

#ifndef PESQUISA_H
#define PESQUISA_H

 /**
 * @brief Função que lê e aloca em um vetor os dados de um arquivo.
 * @param *municipios_para_pesq Ponteiro que aponta para um vetor, no qual será alocado os dados contidos no arquivo.
 * @param qtd_municipios Variavel que recebe a quantidade de municipios que há no arquivo.. 
 */
void pesquisa (int *municipios_para_pesq, int qtd_municipios);

#endif