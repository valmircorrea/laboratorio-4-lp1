/**
 * @file	contador.h
 * @brief	Arquivo com o cabeçario da função contador.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	06/04/2017
 * @date	06/04/2017
 */

#ifndef CONTADOR_H
#define CONTADOR_H

/**
* @brief Função que calcula a quantidade de municipios que estão no arquivo.
* @param argv Vetor de argumentos no qual está o arquivo a ser lido.
* @return retorna a quantidade de municipios.
*/
int contador (char *argv[]);

/**
* @brief Função que calcula a quantidade de municipios que estão no arquivo para a pesquisa.
* @return retorna a quantidade de municipios.
*/
int contador_de_municipios_para_pesq ();

#endif