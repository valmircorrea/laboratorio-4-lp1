/**
 * @file	gera_arquivos.h
 * @brief	Arquivo com o cabeçário da função gera_arquivos.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	08/04/1017
 * @date	09/04/1017
 */

#ifndef GERA_ARQUIVOS_H_
#define GERA_ARQUIVOS_H_

#include "stats.h"

 /**
 * @brief Função que gera arquivos com os resultados dos calculos.
 * @param *maior Ponteiro que recebe um vetor com o maior numero de nascimentos em cada ano.
 * @param *menor Ponteiro que recebe um vetor com o menor numero de nascimentos em cada ano.
 * @param *desvio Ponteiro que recebe um vetor com o desvio padrão do numero de nascimentos em cada ano.
 * @param *total Ponteiro que recebe um vetor com o total de nascimentos em cada ano.
 */
void gera_arquivos (int *maior, int *menor, float *media, float *desvio, int *totais);

 /**
 * @brief Função que gera arquivos com os resultados da pesquisa.
 * @param *municipios_para_pesq Ponteiro que recebe um vetor com os municipios que estão no arquivo 'alvo.dat'.
 * @param qtd_municipios Variavel que recebe a quantidade de municipios que estão no arquivo 'alvo.dat'.
 * @param *tc Ponteiro que recebe um vetor com as taxas de crescimento.
 * @param municipios Variavel que recebe a quantidade de municipios contido no arquivo 'Nascimentos_RN.csv'.
 * @param stats[] Vetor que recebe a strutura de dados dos municipios.
 */
void gera_pesquisa (int *municipios_para_pesq, int qtd_municipios, float *tc, int municipios, Stats stats[]);

#endif