/**
 * @file	leitura.h
 * @brief	Arquivo com o cabeçário da função leitura_arquivo.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	06/04/2017
 * @date	10/04/2017
 */

#ifndef LEITURA_H
#define LEITURA_H

 /**
 * @brief Função que realiza a leitura dos dados de um arquivo.
 * @param *argv Vetor de argumentos no qual está o arquivo a ser lido.
 * @param municipios Variavel que recebe a quantidade de municipios do arquivo.
 * @param stats Vetor de struct para o qual será alocado os dados que serão lidos do arquivo.
 */
void leitura_arquivo (char *argv, int municipios, Stats stats [] );

#endif