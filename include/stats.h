/**
 * @file	stats.h
 * @brief	Arquivo com a estrutura de dados.
 * @author  Valmir Correa (valmircorrea96@outlook.com)
 * @since	08/04/2017
 * @date	08/04/2017
 */

#ifndef STATS_H
#define STATS_H

#include <string>
using std:: string;

struct Stats {
    string codigo;          /**< Código do município*/
    string nome;            /**< Nome do município*/
    int nascimentos[21];    /**< Numero de nascimentos em cada ano contabilizado*/
};

#endif