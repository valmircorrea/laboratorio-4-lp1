/**
 * @file	calculos.h
 * @brief	Cabeçario das funções de calculos.
 * @author Valmir Correa (valmircorrea96@outlook.com)
 * @since	08/04/2017
 * @date	12/04/2017
 */

#ifndef CALCULOS_H
#define CALCULOS_H

 /**
 * @brief Função que calcula o total de nascimentos em cada municipio durante 21 anos.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
void total_nascimentos_municipios (int *total, Stats stats[], int municipios);

 /**
 * @brief Função que calcula o total de nascimentos em cada ano em todos os municipios.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
void total_nascimentos_ano (int *total, Stats stats[], int municipios);

 /**
 * @brief Função que calcula o maior numero de nascimentos no ano em todos os municipios.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
void maior_ano (int *vetor,Stats stats[], int municipios);

  /**
 * @brief Função que calcula o menor numero de nascimentos no ano em todos os municipios.
 * @param *total Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
void menor_ano (int *vetor,Stats stats[], int municipios);

  /**
 * @brief Função que calcula o valor medio do numero de nascimentos no ano em todos os municipios.
 * @param *medias Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
 void media_ano (float *medias, Stats stats[], int municipios);

   /**
 * @brief Função que calcula o desvio padrão do numero de nascimentos no ano em todos os municipios.
 * @param *desvio Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 */
void desvio_padrao_ano (float *media, float *desvio, Stats stats[], int municipios);

 /**
 * @brief Função que calcula a taxa de crescimento relativa de cada municipio.
 * @param *tc Vetor que recebe o vetor para alocar os resultados.
 * @param stats Variavel que recebe o struct com os dados do municipios.
 * @param municipios Variavel que recebe a quantidade de municipios.
 * @param *menor_taxa Ponteiro que recebe o endereço de uma variavel para alocar o valor do calculo da menor taxa.
 * @param *maior_taxa Ponteiro que recebe o endereço de uma variavel para alocar o valor do calculo da maior taxa.
 * @param *indice_maior Ponteiro que recebe o endereço de uma variavel para alocar indice de onde o municipio com a maior taxa esta no struct.
 * @param *indice_menor Ponteiro que recebe o endereço de uma variavel para alocar indice de onde o municipio com a menor taxa esta no struct.
 */
 void taxa (float *tc, Stats stats[], int municipios, float *menor_taxa, float *maior_taxa, int *indice_maior, int *indice_menor);

#endif